import React from 'react';
import QuizAnswer from '../QuizAnswer/QuizAnswer';
import { TextField } from '@material-ui/core';
import { TakeAnswer } from '../../../../models/customTypes';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

type Props = {
  key: number;
  question: { id: number; name: string; points: number; answers: TakeAnswer[] };
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: '70%',
      },
    },
  })
);

const QuizQuestion: React.FC<Props> = ({ question }): JSX.Element => {
  const classes = useStyles();

  return (
    <div>
      <form className={classes.root} noValidate autoComplete='off'>
        <div style={{ margin: 8 }}>
          <TextField
            id='outlined-multiline-flexible'
            label='Question'
            multiline
            rowsMax={4}
            value={question.name}
            variant='filled'
          />
        </div>
      </form>
      {/* <Typography variant='h5' color='inherit' noWrap>
        {question.name}
      </Typography> */}
      {question.answers &&
        question.answers.map((answer) => (
          <QuizAnswer key={answer.id!} answer={answer} />
        ))}
    </div>
  );
};

export default QuizQuestion;
