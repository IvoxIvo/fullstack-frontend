import React from 'react';
import { AccountCircle as AccountCircleIcon } from '@material-ui/icons';
import { useForm } from 'react-hook-form';
import {
  usernameReqs,
  firstnameReqs,
  lastnameReqs,
  passwordReqs,
} from '../../../utils/validation/constants';
import {
  Avatar,
  Container,
  CssBaseline,
  Button,
  Typography,
} from '@material-ui/core';
import { CssTextField, useStyles } from './styles';
import { useHistory } from 'react-router-dom';
import httpProvider from '../../../providers/http-provider';
import { BASE_URL } from '../../../common/constants';
import { displayMessage } from '../../../utils/wrappers/display-message';
import { setToken } from '../../../utils/wrappers/local-storage';
import { useAuth } from '../../../auth/AuthContext';
import decode from 'jwt-decode';

interface RegisterInput {
  username: string;
  firstname: string;
  lastname: string;
  password: string;
  confirmPassword: string;
}

const RegisterForm = () => {
  const classes = useStyles();
  const { register, handleSubmit, errors, watch } = useForm<RegisterInput>();
  const history = useHistory();
  const { setUser } = useAuth();

  const onSubmit = (data: RegisterInput) => {
    const loginData = { username: data.username, password: data.password };

    httpProvider
      .post(`${BASE_URL}/users`, data)
      .then((data) => {
        if (data.message) {
          displayMessage(data.message);
        }
      })
      .catch((error) => console.log(error))
      .finally(() => {
        httpProvider
          .post(`${BASE_URL}/users/signin`, loginData)
          .then((data) => {
            if (data.message) {
              displayMessage(data.message);
            } else {
              setToken(data.token);
              const user = decode(data.token);
              setUser(user);
            }
          });
      });
  };

  return (
    <Container component='main' maxWidth='xs'>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <AccountCircleIcon style={{ fontSize: 45 }} />
        </Avatar>
        <Typography component='h1' variant='h4' style={{ color: '#3F51B5' }}>
          Create an account
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
          <CssTextField
            name='username'
            label='Username'
            variant='outlined'
            margin='normal'
            inputRef={register(usernameReqs)}
            autoComplete='username'
            placeholder='Your username...'
            fullWidth
            required
            autoFocus
          />
          {errors.username && (
            <span className={classes.error}>{errors.username.message}</span>
          )}
          <CssTextField
            name='firstname'
            label='First Name'
            variant='outlined'
            margin='normal'
            inputRef={register(firstnameReqs)}
            autoComplete='firstname'
            placeholder='First name...'
            fullWidth
            required
          />
          {errors.firstname && (
            <span className={classes.error}>{errors.firstname.message}</span>
          )}
          <CssTextField
            name='lastname'
            label='Last Name'
            variant='outlined'
            margin='normal'
            inputRef={register(lastnameReqs)}
            autoComplete='lastname'
            placeholder='Last name...'
            fullWidth
            required
          />
          {errors.lastname && (
            <span className={classes.error}>{errors.lastname.message}</span>
          )}

          <CssTextField
            name='password'
            label='Password'
            type='password'
            variant='outlined'
            margin='normal'
            inputRef={register(passwordReqs)}
            placeholder='Your password...'
            fullWidth
            required
            autoComplete='current-password'
          />
          {errors.password && (
            <span className={classes.error}>{errors.password.message}</span>
          )}
          <CssTextField
            name='confirmPassword'
            label='Confirm Password'
            type='password'
            variant='outlined'
            margin='normal'
            inputRef={register({
              required: true,
              // message: "Passwords don't match",
              validate: (value: string) =>
                value === watch('password') || "Passwords don't match.",
            })}
            placeholder='Confirm password...'
            fullWidth
            required
          />
          {errors.confirmPassword && (
            <span className={classes.error}>
              {errors.confirmPassword.message}
            </span>
          )}

          <Button
            type='submit'
            fullWidth
            variant='contained'
            className={classes.submit}
          >
            Create Account
          </Button>
          <Button
            fullWidth
            variant='contained'
            className={classes.submit}
            onClick={() => history.push('/login')}
          >
            Go back to Login page
          </Button>
        </form>
      </div>
    </Container>
  );
};

export default RegisterForm;
