import { Button } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';
import Categories from '../../SharedComponents/Categories/Categories';
import LastQuizzes from '../LastQuizzes/LastQuizzes';
import './Dashboard.css';

const Dashboard: React.FC = () => {
  return (
    <div>
      <br />
      <div className='dashMain'>
        <div className='dashLeft'>
          <Categories />
        </div>
        <div className='dashRight'>
          <div className='dashRightTButtons'>
            <Link to='/create-category' style={{ textDecoration: 'none' }}>
              <Button
                fullWidth
                variant='contained'
                color='primary'
                size='large'
              >
                New category
              </Button>
            </Link>
            <Link to='/create-quiz' style={{ textDecoration: 'none' }}>
              <Button
                fullWidth
                variant='contained'
                color='primary'
                size='large'
              >
                Create quiz
              </Button>
            </Link>
          </div>
          <div className='dashRightT'>
            <LastQuizzes />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
