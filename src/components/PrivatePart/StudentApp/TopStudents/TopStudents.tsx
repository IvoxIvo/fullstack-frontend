import React, { useEffect } from 'react';
import httpProvider from '../../../../providers/http-provider';
import { BASE_URL } from '../../../../common/constants';
import { TopScores } from '../../../../models/customTypes';
import TopStudentsTable from '../../Containers/TopStudentsTable/TopStudentsTable';

const TopStudents: React.FC = (): JSX.Element => {
  const defaultProps: TopScores[] = [];

  const [students, setStudents]: [
    TopScores[],
    (categories: TopScores[]) => void
  ] = React.useState(defaultProps);

  const [error, setError]: [string, (error: string) => void] = React.useState(
    ''
  );

  useEffect(() => {
    httpProvider
      .get(`${BASE_URL}/scores/top`)
      .then((result) => {
        if (Array.isArray(result)) {
          setStudents([...result]);
        } else {
          throw new Error(result.message);
        }
      })
      .catch((error) => setError(error.message))
      .finally();
  }, []);

  return <TopStudentsTable students={students} />;
};

export default TopStudents;
