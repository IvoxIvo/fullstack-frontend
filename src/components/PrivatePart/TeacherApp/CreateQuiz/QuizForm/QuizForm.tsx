import {
  Button,
  Fab,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Tooltip,
} from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useAuth } from '../../../../../auth/AuthContext';
import Question from '../Question/Question';
import useStyles from '../styles';
import { connect } from 'react-redux';
import httpProvider from '../../../../../providers/http-provider';
import { BASE_URL } from '../../../../../common/constants';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';
import HelpIcon from '@material-ui/icons/Help';
import { useHistory } from 'react-router-dom';
import { quizFormProps } from '../../../../../models/quizTypes';
import * as actionTypes from '../../../../../store/actionTypes';

const QuizForm: React.FC<quizFormProps> = (props) => {
  const { user } = useAuth();
  const classes = useStyles();
  const history = useHistory();
  const [categoryName, setCategoryName] = useState<string>('');
  const [categories, setCategories] = useState<any[]>([]);
  const [existingQuizzes, setExistingQuizzes] = useState<string[]>([]);
  const [questions, setQuestions] = useState<JSX.Element[] | null>([]);
  const [isQuizValid, setIsQuizValid] = useState<boolean>(false);
  const [title, setTitle] = useState('');
  const [validTitle, setValidTitle] = useState(false);
  const [uniqueTitle, setUniqueTitle] = useState(true);
  const [timelimit, setTimelimit] = useState(0);
  const [validTimelimit, setValidTimelimit] = useState(false);
  const {
    quiz,
    updateQuizCategoryId,
    updateQuizTitle,
    updateQuizTimelimit,
    updateUserId,
    addQuizQuestion,
    removeQuizLastQuestion,
    setDuplicateQuizName,
    resetState,
  } = props;

  useEffect(() => {
    updateUserId(user?.sub);
    const fetchCategoryNames = httpProvider.get(`${BASE_URL}/categories`);
    const fetchQuizNames = httpProvider.get(`${BASE_URL}/quiz`);

    Promise.all([fetchCategoryNames, fetchQuizNames]).then((resolved) => {
      const categoryNames = resolved[0].category.map((cat: any) => cat);
      if (!categoryNames.length) {
        history.push('/dashboard');
      }
      setCategories(categoryNames);
      const quizNames = resolved[1].map((quiz: any) => quiz.title);
      setExistingQuizzes(quizNames);
    });
  }, [updateUserId, user?.sub]);

  useEffect(() => {
    const quizValids: (boolean | undefined)[] = Object.values(quiz.valid);

    const nestedValids: (boolean | undefined)[] = quiz.questions.reduce(
      (acc, question) => {
        acc = acc.concat(Object.values(question.valid));
        question.answers.forEach(
          (answer) => (acc = acc.concat(Object.values(answer.valid)))
        );
        return acc;
      },
      quizValids
    );

    const areAllValid: boolean = nestedValids.every((value) => value === true);
    setIsQuizValid(areAllValid);
  }, [quiz]);

  // SUBMIT & CANCEL
  const handleSubmit = () => {
    httpProvider
      .post(`${BASE_URL}/quiz`, quiz)
      .then((data) => console.log(data))
      .catch((error) => console.log(error))
      .finally(() => {
        history.push('/dashboard');
        resetState();
      });
  };

  const handleCancel = () => {
    resetState();
    history.push('/dashboard');
  };

  // ON CHANGE EVENT
  const handleCategoryChange = (
    event: React.ChangeEvent<{ value: unknown }>
  ) => {
    setCategoryName(event.target.value as string);
    updateQuizCategoryId(event.target.value);
  };

  const handleTimelimitChange = (
    event: React.ChangeEvent<{ value: string }>
  ) => {
    const timelimit = +event.target.value;
    setTimelimit(timelimit);
    if (timelimit >= 0) {
      setValidTimelimit(true);
      updateQuizTimelimit({ timelimit: timelimit, valid: true });
    } else {
      setValidTimelimit(false);
      updateQuizTimelimit({ timelimit: timelimit, valid: false });
    }
  };

  const handleTitleChange = (event: React.ChangeEvent<{ value: string }>) => {
    const title = event.target.value;
    const duplicate = existingQuizzes.some(
      (quiz) => quiz === event.target.value
    );
    setTitle(title);
    if (title.length > 4 && title.length < 256) {
      setValidTitle(true);
      updateQuizTitle({ title: title, valid: true });
    } else {
      setValidTitle(false);
      updateQuizTitle({ title: title, valid: false });
    }
    if (duplicate) {
      setUniqueTitle(false);
      setDuplicateQuizName({ valid: false });
    } else {
      setUniqueTitle(true);
      setDuplicateQuizName({ valid: true });
    }
  };

  // LOCAL FUNCTIONS
  const handleQuestions = (questionType: string) => {
    if (questionType === 'single' && questions) {
      addQuizQuestion({ questionId: questions.length + 1 });
      setQuestions([
        ...questions,
        <Question
          questionId={questions.length + 1}
          questionType={questionType}
        />,
      ]);
    } else if (questionType === 'multi' && questions) {
      setQuestions([
        ...questions,
        <Question
          questionId={questions.length + 1}
          questionType={questionType}
        />,
      ]);
      addQuizQuestion({ questionId: questions.length + 1 });
    }
  };

  const removeLastQuestion = () => {
    if (questions) {
      setQuestions(questions.slice(0, -1));
      removeQuizLastQuestion();
    }
  };

  return (
    <div>
      <br />
      <Grid container spacing={1} justify='center'>
        <Grid item>
          <InputLabel id='select-category'>Select Category</InputLabel>
          <Select
            className={classes.category}
            name='category'
            labelId='select-category'
            id='demo-simple-select'
            value={categoryName}
            required
            variant='outlined'
            onChange={handleCategoryChange}
          >
            {categories &&
              categories.map((cat) => (
                <MenuItem key={cat.name} value={cat.id}>
                  {cat.name}
                </MenuItem>
              ))}
          </Select>
          {categoryName ? (
            <DoneOutlineIcon className={classes.iconDone} />
          ) : (
            <PriorityHighIcon className={classes.iconError} />
          )}
        </Grid>
        <Grid item>
          <InputLabel id='timelimit'>Timelimit (minutes)</InputLabel>
          <TextField
            className={classes.timelimit}
            name='timelimit'
            id='timelimit'
            type='number'
            value={timelimit}
            required
            variant='outlined'
            onChange={handleTimelimitChange}
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{ min: '0' }}
          />
          {timelimit === 0 ? (
            <Tooltip title='Set this to 0 if you want the quiz to have no time limit!'>
              <HelpIcon className={classes.iconInfo} />
            </Tooltip>
          ) : validTimelimit ? (
            <DoneOutlineIcon className={classes.iconDone} />
          ) : (
            <PriorityHighIcon className={classes.iconError} />
          )}
        </Grid>
      </Grid>
      <TextField
        className={classes.root}
        required
        name='title'
        value={title}
        id='title'
        multiline
        label='Quiz Title...'
        variant='outlined'
        onChange={handleTitleChange}
        helperText={
          !validTitle ? (
            <span className={classes.errorMessage}>
              Quiz title must be between 5 and 256 characters!
            </span>
          ) : !uniqueTitle ? (
            <span className={classes.errorMessage}>
              Quiz with the same name already exists!
            </span>
          ) : null
        }
      />
      {validTitle ? (
        <DoneOutlineIcon className={classes.iconDone} />
      ) : (
        <PriorityHighIcon className={classes.iconError} />
      )}
      <Grid
        container
        spacing={1}
        direction='column'
        justify='center'
        className={classes.root}
      >
        {questions &&
          questions.map((question, index) => (
            <Grid item key={index + 2}>
              <div>{question}</div>
              {!quiz.questions[index].valid.correctAnswers && (
                <p className={classes.errorMessage}>
                  Please, mark the necessary correct answers!
                </p>
              )}
            </Grid>
          ))}
      </Grid>
      <br />
      {questions && questions.length < 2 && (
        <div>
          <span className={classes.errorMessage}>
            Quiz must contain at least 2 questions!
          </span>
        </div>
      )}
      <div className={classes.root}>
        <Button
          variant='contained'
          value='single'
          color='primary'
          onClick={() => handleQuestions('single')}
        >
          Add Single Answer Question
        </Button>
        &nbsp;
        <Button
          variant='contained'
          value='multi'
          color='primary'
          onClick={() => handleQuestions('multi')}
        >
          Add Multi Answer Question
        </Button>
      </div>
      {questions && questions.length > 0 && (
        <Button
          variant='contained'
          value='multi'
          color='primary'
          onClick={removeLastQuestion}
        >
          Remove Last Question
        </Button>
      )}
      <Fab
        className={classes.submitButtonStyle}
        variant='extended'
        color='primary'
        onClick={handleSubmit}
        disabled={!isQuizValid}
      >
        Submit Quiz
      </Fab>
      <Fab
        className={classes.cancelButtonStyle}
        variant='extended'
        color='secondary'
        onClick={handleCancel}
      >
        Cancel
      </Fab>
    </div>
  );
};

const mapStateToProps = (state: any) => {
  return {
    quiz: state.quiz,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    updateQuizCategoryId: (value: number) =>
      dispatch({ type: actionTypes.QUIZ_UPDATE_CATEGORY_ID, value }),
    updateQuizTitle: (value: { title: string; valid: boolean }) =>
      dispatch({ type: actionTypes.QUIZ_UPDATE_TITLE, value }),
    updateQuizTimelimit: (value: { timelimit: number; valid: boolean }) =>
      dispatch({ type: actionTypes.QUIZ_UPDATE_TIMELIMIT, value }),
    updateUserId: (value: number) =>
      dispatch({ type: actionTypes.QUIZ_UPDATE_USERID, value }),
    addQuizQuestion: (value: { questionId: number; questionType: number }) =>
      dispatch({ type: actionTypes.QUIZ_ADD_QUESTION, value }),
    removeQuizLastQuestion: () =>
      dispatch({ type: actionTypes.QUIZ_REMOVE_LAST_QUESTION }),
    setDuplicateQuizName: (value: { valid: boolean }) =>
      dispatch({ type: actionTypes.QUIZ_DUPLICATE_NAME, value }),
    resetState: () => dispatch({ type: actionTypes.RESET_STATE }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(QuizForm);
