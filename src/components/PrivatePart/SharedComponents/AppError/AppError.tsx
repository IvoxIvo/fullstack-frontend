import React from 'react';
import PropTypes from 'prop-types';
type Props = {
  message: string | undefined;
};
const AppError: React.FC<Props> = ({ message }): JSX.Element => {
  return (
    <div className='AppError'>
      <h1>{message}</h1>
    </div>
  );
};

AppError.propTypes = {
  message: PropTypes.string.isRequired,
};

export default AppError;
