import { Checkbox, Grid, TextField } from '@material-ui/core';
import React, { useState } from 'react';
import useStyles from '../styles';
import { connect } from 'react-redux';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';
import { answerProps } from '../../../../../models/quizTypes';
import * as actionTypes from '../../../../../store/actionTypes';

const Answer: React.FC<answerProps> = (props) => {
  const classes = useStyles();
  const [checked, setChecked] = useState(false);
  const [text, setText] = useState('');
  const [validText, setValidText] = useState(false);
  const {
    answerId,
    questionId,
    answerType,
    addAnswerText,
    setSingleAnswerCorrect,
    setMultiAnswerCorrect,
    validateCorrectAnswers,
  } = props;

  // ON CHANGE EVENT
  const handleCheckbox = (event: React.ChangeEvent<HTMLInputElement>) => {
    const checked = event.target.checked;
    setChecked(checked);
    setMultiAnswerCorrect({
      questionId: questionId,
      answerId: answerId,
      is_correct: +checked,
    });
    validateCorrectAnswers({
      questionId: questionId,
      questionType: answerType,
    });
  };

  const localText = (event: React.ChangeEvent<HTMLInputElement>) => {
    const text = event.target.value;
    setText(text);
    if (!text.length || text.length > 256) {
      setValidText(false);
      addAnswerText({
        questionId: questionId,
        answerId: answerId,
        text: text,
        valid: false,
      });
    } else {
      setValidText(true);
      addAnswerText({
        questionId: questionId,
        answerId: answerId,
        text: text,
        valid: true,
      });
    }
  };

  // LOCAL FUNCTIONS
  const handleRadioButton = () => {
    setSingleAnswerCorrect({ questionId: questionId, answerId: answerId });
    validateCorrectAnswers({
      questionId: questionId,
      questionType: answerType,
    });
  };

  return (
    <div>
      <Grid container spacing={1} justify='center'>
        <Grid item>
          {answerType === 'single' ? (
            <input
              className={classes.radioButton}
              type='radio'
              name={`a-${questionId}`}
              onChange={handleRadioButton}
              color='primary'
            />
          ) : (
            <Checkbox
              checked={checked}
              onChange={handleCheckbox}
              color='primary'
            />
          )}
        </Grid>
        <Grid item>
          <TextField
            className={classes.root}
            label='answer...'
            size='small'
            multiline
            value={text || ''}
            onChange={localText}
            helperText={
              validText ? null : (
                <span className={classes.errorMessage}>
                  Answer text must be between 1 and 256 characters!
                </span>
              )
            }
          />
          {validText ? (
            <DoneOutlineIcon className={classes.iconDone} />
          ) : (
            <PriorityHighIcon className={classes.iconError} />
          )}
        </Grid>
      </Grid>
    </div>
  );
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    addAnswerText: (value: {
      questionId: number;
      answerId: number;
      text: string;
      valid: boolean;
    }) => dispatch({ type: actionTypes.ANSWER_ADD_TEXT, value }),
    setMultiAnswerCorrect: (value: {
      questionId: number;
      answerId: number;
      is_correct: number;
    }) => dispatch({ type: actionTypes.ANSWER_SET_MULTI_CORRECT, value }),
    setSingleAnswerCorrect: (value: { questionId: number; answerId: number }) =>
      dispatch({ type: actionTypes.ANSWER_SET_SINGLE_CORRECT, value }),
    validateCorrectAnswers: (value: {
      questionId: number;
      questionType: string;
    }) => dispatch({ type: actionTypes.VALIDATE_CORRECT_ANSWERS, value }),
  };
};

export default connect(null, mapDispatchToProps)(Answer);
