import React, { useEffect } from 'react';
import httpProvider from '../../../../providers/http-provider';
import { BASE_URL } from '../../../../common/constants';
import LastTakeTable from '../../Containers/LastTakeTable/LastTakeTable';
import { LastTake } from '../../../../models/customTypes';
import AppError from '../../SharedComponents/AppError/AppError';
import Loading from '../../SharedComponents/Loading/Loading';

const LastTakes: React.FC = (): JSX.Element => {
  const defaultProps: LastTake[] = [];

  const [takes, setTakes]: [
    LastTake[],
    (categories: LastTake[]) => void
  ] = React.useState(defaultProps);

  const [loading, setLoading]: [
    boolean,
    (loading: boolean) => void
  ] = React.useState<boolean>(true);

  const [error, setError]: [string, (error: string) => void] = React.useState(
    ''
  );

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get(`${BASE_URL}/takes/student/last`)
      .then((result) => {
        if (Array.isArray(result)) {
          setTakes([...result]);
        } else {
          throw new Error(result.message);
        }
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  if (error) {
    return (
      <div>
        <AppError message={error} />
      </div>
    );
  }

  if (loading) {
    return (
      <div>
        <Loading />
      </div>
    );
  }

  return <LastTakeTable takes={takes} />;
};

export default LastTakes;
