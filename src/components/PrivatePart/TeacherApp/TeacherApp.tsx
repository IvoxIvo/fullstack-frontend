import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import CreateCategory from './CreateCategory/CreateCategory';
import QuizForm from './CreateQuiz/QuizForm/QuizForm';
import SolveQuiz from '../StudentApp/SolveQuiz/SolveQuiz';
import SingleCategory from '../StudentApp/SingleCategory/SingleCategory';
import Dashboard from './Dashboard/Dashboard';
import NavBar from '../SharedComponents/NavBar/NavBar';
import ViewQuizInfo from './QuizInfo/ViewQuizInfo';
import Leaderboard from '../SharedComponents/Leaderboard/Leaderboard';

const TeacherApp: React.FC = () => {
  return (
    <BrowserRouter>
      <NavBar />
      <div className='main-content'>
        <Switch>
          <Redirect path='/' exact to='/dashboard' />
          <Redirect path='/login' exact to='/dashboard' />
          <Route path='/dashboard' component={Dashboard} />
          <Route path='/create-category' component={CreateCategory} />
          <Route path='/create-quiz' component={QuizForm} />
          <Route path='/category/:id' component={SingleCategory} />
          <Route path='/info/quiz/:id' component={ViewQuizInfo} />
          <Route path='/leaderboard' component={Leaderboard} />
          <Route path='/quiz/:id' component={SolveQuiz} />
          <Route path='*' component={Dashboard} />
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export default TeacherApp;
