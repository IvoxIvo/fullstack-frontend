import React, { useEffect } from 'react';
import httpProvider from '../../../../providers/http-provider';
import QuizzesOfCategory from '../QuizzesOfCategory/QuizzesOfCategory';
import { BASE_URL } from '../../../../common/constants';
import CategoryInfo from '../../Containers/CategoryInfo/CategoryInfo';
import { ExtractId } from '../../../../utils/ExtractId';
import { Button } from '@material-ui/core';
import { useHistory } from 'react-router';
import AppError from '../../SharedComponents/AppError/AppError';

const Category: React.FC = (): JSX.Element => {
  const history = useHistory();
  const [categoryData, setCategoryData]: [
    {
      id: number;
      users_id: number;
      description: string;
      name: string;
      path: string;
    },
    (categories: {
      id: number;
      users_id: number;
      description: string;
      name: string;
      path: string;
    }) => void
  ] = React.useState({
    id: 0,
    users_id: 0,
    description: '',
    name: '',
    path: '',
  });

  const [error, setError]: [string, (error: string) => void] = React.useState(
    ''
  );

  const id: string = ExtractId();

  useEffect(() => {
    httpProvider
      .get(`${BASE_URL}/categories/${id}`)
      .then((result) => {
        setCategoryData(result);
      })
      .catch((error) => setError(error.message))
      .finally();
  }, [id]);

  if (error) {
    return (
      <div>
        <AppError message={error} />
      </div>
    );
  }

  return (
    <div className='dashMain'>
      <div className='dashLeft'>
        <QuizzesOfCategory key={id} id={id} />
        <Button
          style={{ margin: '6px 0 15px 0' }}
          variant='contained'
          color='primary'
          onClick={() => history.goBack()}
        >
          Back to dashboard
        </Button>
      </div>
      <div className='dashRight'>
        <div className='dashRightT'>
          <CategoryInfo key={categoryData.id} category={categoryData} />
        </div>
      </div>
    </div>
  );
};

export default Category;
