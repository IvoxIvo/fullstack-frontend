import React, { useEffect } from 'react';
import httpProvider from '../../../../providers/http-provider';
import { BASE_URL } from '../../../../common/constants';
import CategoryCard from '../../Containers/CategoryCard/CategoryCard';
import MainItemContainer from '../../Containers/MainItemContainer/MainItemContainer';
import { Category } from '../../../../models/customTypes';

const Categories: React.FC = (): JSX.Element => {
  const defaultProps: Category[] = [];

  const [categories, setCategories]: [
    Category[],
    (categories: Category[]) => void
  ] = React.useState(defaultProps);

  const [error, setError]: [string, (error: string) => void] = React.useState(
    ''
  );

  useEffect(() => {
    httpProvider
      .get(`${BASE_URL}/categories/all`)
      .then((result) => {
        if (Array.isArray(result)) {
          setCategories([...result]);
        } else {
          throw new Error(result.message);
        }
      })
      .catch((error) => setError(error.message))
      .finally();
  }, []);

  return (
    <MainItemContainer>
      {categories &&
        categories.map((category) => (
          <CategoryCard key={category.id} category={category} />
        ))}
    </MainItemContainer>
  );
};

export default Categories;
