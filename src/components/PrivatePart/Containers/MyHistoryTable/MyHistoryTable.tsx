import React from 'react';
import { UserHistory } from '../../../../models/customTypes';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';

type Props = {
  props: UserHistory;
};

const useStyles = makeStyles({
  table: {
    width: '100%',
  },
  header: {
    width: '100%',
    paddingTop: '15px',
    marginLeft: '225%',
  },
});

const MyHistoryTable: React.FC<Props> = ({ props }): JSX.Element => {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label='a dense table'>
        <TableHead>
          <Typography className={classes.header} noWrap variant='h6'>
            Your Results
            <hr style={{ width: '115px' }} />
          </Typography>
          <TableRow>
            <TableCell align='left'>User name</TableCell>
            <TableCell align='left'>First name</TableCell>
            <TableCell align='left'>Last name</TableCell>
            <TableCell align='left'>Score</TableCell>
            <TableCell align='left'>Quiz Title</TableCell>
            <TableCell align='left'>Taken on</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.result.map((student) => (
            <TableRow key={student.username}>
              <TableCell component='th' scope='row'>
                {student.username}
              </TableCell>
              <TableCell align='left'>{student.firstname}</TableCell>
              <TableCell align='left'>{student.lastname}</TableCell>
              <TableCell align='left'>{student.result}</TableCell>
              <TableCell align='left'>{student.title}</TableCell>
              <TableCell align='left'>
                {new Date(student.started_at).toDateString()}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default MyHistoryTable;
