import { Card, CardContent, CardMedia } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { BASE_URL } from '../../../../common/constants';
import httpProvider from '../../../../providers/http-provider';

type categoryImage = {
  id: number;
  name: string;
  path: string;
};

const useStyles = makeStyles({
  card: {
    padding: '4px',
    margin: '4px',
    display: 'inline-block',
    flex: '1 0 13.4%',
    boxSizing: 'border-box',
  },
  title: {
    marginTop: '1px',
  },
  radio: {
    border: '0px',
    width: '100%',
    height: '1.5em',
  },
});

const CategoryImages = (props: any) => {
  const classes = useStyles();
  const [images, setImages] = useState<categoryImage[] | undefined>([]);
  const { handleImageSelect } = props;
  useEffect(() => {
    httpProvider.get(`${BASE_URL}/categories/images`).then((data) => {
      setImages(data);
    });
  }, []);

  return (
    <>
      {images &&
        images.map((image: categoryImage) => {
          return (
            <Card key={image.id} className={classes.card}>
              <CardContent className={classes.title}>{image.name}</CardContent>
              <CardMedia
                style={{ height: 0, paddingTop: '100%', marginBottom: '5px' }}
                image={`${BASE_URL}${image.path}`}
                title={image.name}
              />
              {image.id === 1 ? (
                <input
                  className={classes.radio}
                  type='radio'
                  color='primary'
                  name='category-images'
                  value={image.id}
                  defaultChecked
                  onChange={() => handleImageSelect(image.id)}
                />
              ) : (
                <input
                  className={classes.radio}
                  type='radio'
                  color='primary'
                  name='category-images'
                  value={image.id}
                  onChange={() => handleImageSelect(image.id)}
                />
              )}
            </Card>
          );
        })}
    </>
  );
};
export default CategoryImages;
