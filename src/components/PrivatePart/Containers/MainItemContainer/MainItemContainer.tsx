import React from 'react';
import Box from '@material-ui/core/Box';

type Props = {
  children: JSX.Element[] | JSX.Element;
};

const CategoryContainer: React.FC<Props> = (props): JSX.Element => {
  return (
    <div style={{ width: '100%' }}>
      <Box display='flex' flexWrap='wrap'>
        {props.children}
      </Box>
    </div>
  );
};

export default CategoryContainer;
