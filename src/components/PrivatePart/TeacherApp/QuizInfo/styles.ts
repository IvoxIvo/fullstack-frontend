import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  table: {
    width: '100%',
    margin: 'auto',
  },
  tableTitle: {
    fontWeight: 700,
  },
  tableContainer: {
    width: '50%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '25px',
  },
});

export default useStyles;
