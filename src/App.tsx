import React, { useState } from 'react';
import decode from 'jwt-decode';
import AuthContext from './auth/AuthContext';
import './App.css';
import TeacherApp from './components/PrivatePart/TeacherApp/TeacherApp';
import PublicApp from './components/PublicPart/PublicApp';
import StudentApp from './components/PrivatePart/StudentApp/StudentApp';
import { getToken } from './utils/wrappers/local-storage';

interface IDecodedToken {
  sub: number;
  username: string;
  role: string;
  iat: number;
  exp: number;
}

const App: React.FC = () => {
  const token = getToken();
  const [user, setUser] = useState<IDecodedToken | null>(
    token ? decode(token) : null
  );

  return (
    <div className='App'>
      <AuthContext.Provider value={{ user: user, setUser: setUser }}>
        {user ? (
          user.role === 'teacher' ? (
            <TeacherApp />
          ) : (
            <StudentApp />
          )
        ) : (
          <PublicApp />
        )}
      </AuthContext.Provider>
    </div>
  );
};

export default App;
