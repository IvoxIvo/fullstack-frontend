import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import { useAuth } from '../../../../auth/AuthContext';
import { useHistory } from 'react-router-dom';
import { IconButton } from '@material-ui/core';
import { AccountCircle } from '@material-ui/icons';
import { BASE_URL } from '../../../../common/constants';
import useStyles from './styles';
import { removeToken } from '../../../../utils/wrappers/local-storage';

const NavBar: React.FC = (): JSX.Element => {
  const classes = useStyles();
  const { user, setUser } = useAuth();
  const history = useHistory();

  const logout = () => {
    removeToken();
    setUser(null);
    history.push('/login');
  };

  return (
    <div className={classes.root}>
      <AppBar position='static'>
        <Toolbar>
          <img src={`${BASE_URL}/logo/logo.png`} alt='logo' />
          <Button color='inherit' onClick={() => history.push('/dashboard')}>
            Dashboard
          </Button>
          <Button
            color='inherit'
            onClick={() => history.push('/leaderboard?page=1')}
          >
            Leaderboard
          </Button>
          <div className={classes.user}>
            {user?.username}
            <IconButton
              aria-label='account of current user'
              aria-controls='menu-appbar'
              onClick={() => history.push('/myhistory?page=1')}
            >
              <AccountCircle />
            </IconButton>
            <Button color='inherit' onClick={logout}>
              Logout
            </Button>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default NavBar;
