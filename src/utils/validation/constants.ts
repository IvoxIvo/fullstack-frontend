export const usernameReqs = {
  required: true,
  pattern: {
    value: /^(?=.{3,25}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/,
    message: 'Username must be between 3 and 25 characters (including)',
  },
};

export const firstnameReqs = {
  required: true,
  pattern: {
    value: /^(?=.{2,25}$)[a-zA-Z._]+(?<![_.])$/,
    message:
      'First name must be only letters between 2 and 25 characters (including)',
  },
};

export const lastnameReqs = {
  required: true,
  pattern: {
    value: /^(?=.{2,25}$)[a-zA-Z._]+(?<![_.])$/,
    message:
      'Last name must be only letters between 2 and 25 characters (including)',
  },
};

export const passwordReqs = {
  required: true,
  pattern: {
    value: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[a-zA-Z]).{4,25}$/,
    message:
      'Password must include at least one lower-case and one upper-case letter and at least one digit. Length: 4-25 characters',
  },
};
