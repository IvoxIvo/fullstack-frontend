import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import SolveDialog from '../SolveDialog/SolveDialog';
import { useAuth } from '../../../../auth/AuthContext';
import { QuizCardProps } from '../../../../models/customTypes';
import { ButtonGroup } from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    margin: 8,
    flex: '1 0 31%',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

const QuizCard: React.FC<QuizCardProps> = ({ quiz }): JSX.Element => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const { user } = useAuth();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Card className={classes.root}>
      <SolveDialog
        open={open}
        handleClose={handleClose}
        quizId={quiz.quiz_id}
      />

      <CardContent>
        <Typography
          className={classes.title}
          color='textSecondary'
          gutterBottom
        >
          Quiz
        </Typography>
        <Typography variant='h5' component='h2'>
          {quiz.title}
        </Typography>
        <Typography className={classes.pos} color='textSecondary'>
          {user?.role === 'student' && quiz.taken_quiz === 1
            ? 'Quiz is submitted'
            : 'Quiz is open'}
        </Typography>
      </CardContent>
      <CardActions>
        {user?.role === 'teacher' || quiz.taken_quiz === 0 ? (
          <Button
            style={{
              width: '100%',
            }}
            onClick={() => handleClickOpen()}
            variant='contained'
            color='primary'
            size='small'
          >
            Take Quiz
          </Button>
        ) : (
          <Button
            variant='contained'
            color='primary'
            size='small'
            disabled
            style={{
              marginLeft: 'auto',
              marginRight: 'auto',
            }}
          >
            QUIZ ALREADY TAKEN
          </Button>
        )}
        {user?.role === 'teacher' && (
          <ButtonGroup fullWidth>
            <Link
              to={`/info/quiz/${quiz.quiz_id}`}
              style={{
                textDecoration: 'none',
                color: 'black',
                width: '100%',
              }}
            >
              <Button
                variant='contained'
                color='primary'
                size='small'
                style={{
                  width: '100%',
                  textDecoration: 'none',
                }}
              >
                VIEW QUIZ
              </Button>
            </Link>
          </ButtonGroup>
        )}
      </CardActions>
    </Card>
  );
};

export default QuizCard;
