import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 480,
    },
    selectEmpty: {
      marginTop: theme.spacing(1),
    },
    root: {
      margin: theme.spacing(1),
      minWidth: 480,
    },
    points: {
      margin: theme.spacing(1),
      minWidth: 80,
    },
    error: {
      color: 'red',
    },
    iconDone: {
      color: green[500],
      paddingTop: '1.5rem',
      position: 'absolute',
    },
    iconError: {
      color: 'red',
      paddingTop: '1.5rem',
      position: 'absolute',
    },
  })
);

export default useStyles;
