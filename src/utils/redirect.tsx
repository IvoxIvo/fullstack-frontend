import { JsxElement } from 'typescript';

export const redirectTo = (path: string): void => {
  window.location.assign(`${path}`);
};
