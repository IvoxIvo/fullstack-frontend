import React from 'react';
import Box from '@material-ui/core/Box';

type Props = {
  children: JSX.Element[] | JSX.Element;
};

const CreateCategoryContainer: React.FC<Props> = (props): JSX.Element => {
  return (
    <div
      style={{
        padding: '4px',
        backgroundColor: '#eee',
        borderRadius: '8px',
        width: '50%',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: '25px',
        marginBottom: '15px',
      }}
    >
      <Box display='flex' flexWrap='wrap'>
        {props.children}
      </Box>
    </div>
  );
};

export default CreateCategoryContainer;
