import React from 'react';
interface IDecodedToken {
  sub: number;
  username: string;
  role: string;
  iat: number;
  exp: number;
}
interface IAuthContext {
  user: IDecodedToken | null;
  setUser: Function;
}
const AuthContext = React.createContext<IAuthContext>({
  user: null,
  setUser: () => {},
});

export const useAuth = () => React.useContext(AuthContext);

export default AuthContext;
