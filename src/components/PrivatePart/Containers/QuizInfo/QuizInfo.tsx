import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Fab } from '@material-ui/core';
import QuizTimer from '../QuizTimer/QuizTimer';
import { QuizInfoProps } from '../../../../models/customTypes';

const useStyles = makeStyles({
  root: {
    margin: 2,
    width: 'auto',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

const style: any = {
  margin: 0,
  top: '0',
  right: 20,
  bottom: 'auto',
  left: 'auto',
  position: 'sticky',
};

const QuizInfo: React.FC<QuizInfoProps> = ({
  title,
  questions,
  timelimit,
  totalTime,
  showtimer,
}): JSX.Element => {
  const classes = useStyles();
  let totalPoints: number = 0;

  if (questions) {
    totalPoints = questions.reduce((acc, question) => {
      acc += question.points;
      return acc;
    }, 0);
  }

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography
          className={classes.title}
          color='textSecondary'
          gutterBottom
        >
          Quiz Title:
        </Typography>
        <Typography variant='h5' component='h2'>
          {title}
        </Typography>
        <Typography className={classes.pos} color='textSecondary'>
          Total points: {totalPoints}
        </Typography>
        <div>
          {showtimer ? (
            <Fab style={style}>
              <QuizTimer timelimit={timelimit} totalTime={totalTime} />
            </Fab>
          ) : null}
        </div>
      </CardContent>
    </Card>
  );
};

export default QuizInfo;
