import { Button, Grid, TextField } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import Answer from '../Answer/Answer';
import useStyles from '../styles';
import { connect } from 'react-redux';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';
import { valid, answer, questionProps } from '../../../../../models/quizTypes';
import * as actionTypes from '../../../../../store/actionTypes';

const Question: React.FC<questionProps> = (props) => {
  const classes = useStyles();
  const [name, setName] = useState('');
  const [validName, setValidName] = useState(false);
  const [points, setPoints] = useState(1);
  const [validPoints, setValidPoints] = useState(true);
  const {
    questionId,
    questionType,
    addQuestionAnswer,
    addQuestionPoints,
    addQuestionTitle,
    removeQuestionLastAnswer,
    validateCorrectAnswers,
  } = props;

  const [answers, setAnswers] = useState([
    <div key={1}>
      <Answer answerId={1} questionId={questionId} answerType={questionType} />
    </div>,
    <div key={2}>
      <Answer answerId={2} questionId={questionId} answerType={questionType} />
    </div>,
  ]);

  useEffect(() => {
    addQuestionAnswer({ questionId: questionId, answerId: 1 });
    addQuestionAnswer({ questionId: questionId, answerId: 2 });
  }, [addQuestionAnswer, questionId]);

  // ON CHANGE EVENT
  const handleName = (event: React.ChangeEvent<{ value: string }>) => {
    const name = event.target.value;
    setName(name);
    if (name.length < 2 || name.length > 256) {
      setValidName(false);
      addQuestionTitle({ questionId: questionId, name: name, valid: false });
    } else {
      setValidName(true);
      addQuestionTitle({ questionId: questionId, name: name, valid: true });
    }
  };

  const handlePoints = (event: React.ChangeEvent<{ value: string }>) => {
    const points = +event.target.value;
    setPoints(points);
    if (points < 1 || points > 6) {
      setValidPoints(false);
      addQuestionPoints({
        questionId: questionId,
        points: points,
        valid: false,
      });
    } else {
      setValidPoints(true);
      addQuestionPoints({
        questionId: questionId,
        points: points,
        valid: true,
      });
    }
  };

  // LOCAL FUNCTIONS
  const handleAnswers = () => {
    setAnswers([
      ...answers,
      <Answer
        answerId={answers.length + 1}
        questionId={questionId}
        answerType={questionType}
      />,
    ]);
    addQuestionAnswer({ questionId: questionId, answerId: answers.length + 1 });
  };

  const removeLastAnswer = () => {
    if (answers.length > 2) {
      setAnswers(answers.slice(0, -1));
      removeQuestionLastAnswer(questionId);
      validateCorrectAnswers({
        questionId: questionId,
        questionType: questionType,
      });
    }
  };

  return (
    <div className='answers'>
      <div className={classes.p}>{`Question ${questionId}`}</div>
      <hr className={classes.hr} />
      <Grid container spacing={1} justify='center'>
        <Grid item>
          <TextField
            className={classes.question}
            required
            name='name'
            id='name'
            multiline
            value={name}
            onChange={handleName}
            label='Question text...'
            variant='outlined'
            helperText={
              !validName && (
                <span className={classes.errorMessage}>
                  Question name must be between 1 and 256 characters!
                </span>
              )
            }
          />
          {validName ? (
            <DoneOutlineIcon className={classes.iconDone} />
          ) : (
            <PriorityHighIcon className={classes.iconError} />
          )}
        </Grid>
        <Grid item>
          <TextField
            className={classes.points}
            name='points'
            id='points'
            label='Points'
            type='number'
            value={points}
            required
            variant='outlined'
            onChange={handlePoints}
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{ min: '1', max: '6' }}
          />
          {validPoints ? (
            <DoneOutlineIcon className={classes.iconDone} />
          ) : (
            <PriorityHighIcon className={classes.iconError} />
          )}
        </Grid>
      </Grid>
      {answers &&
        answers.map((answer, index) => <div key={index + 2}>{answer}</div>)}
      <Button
        size='small'
        variant='outlined'
        color='primary'
        onClick={handleAnswers}
      >
        Add Answer
      </Button>
      &nbsp;
      {answers.length > 2 && (
        <Button
          size='small'
          variant='outlined'
          color='primary'
          onClick={removeLastAnswer}
        >
          Remove Last
        </Button>
      )}
    </div>
  );
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    addQuestionAnswer: (value: { questionId: number; answerId: number }) =>
      dispatch({ type: actionTypes.QUESTION_ADD_ANSWER, value }),
    addQuestionPoints: (value: {
      questionId: number;
      points: number;
      valid: boolean;
    }) => dispatch({ type: actionTypes.QUESTION_ADD_POINTS, value }),
    addQuestionTitle: (value: {
      questionId: number;
      name: string;
      valid: boolean;
    }) => dispatch({ type: actionTypes.QUESTION_ADD_TITLE, value }),
    removeQuestionLastAnswer: (value: number) =>
      dispatch({ type: actionTypes.QUESTION_REMOVE_LAST_ANSWER, value }),
    validateCorrectAnswers: (value: {
      questionId: number;
      questionType: string;
    }) => dispatch({ type: actionTypes.VALIDATE_CORRECT_ANSWERS, value }),
  };
};

export default connect(null, mapDispatchToProps)(Question);
