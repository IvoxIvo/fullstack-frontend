import React, { useEffect } from 'react';
import httpProvider from '../../../../providers/http-provider';
import { BASE_URL } from '../../../../common/constants';
import QuizQuestion from '../../Containers/QuizQuestion/QuizQestion';
import { Button, ButtonGroup, FormControl } from '@material-ui/core';
import { connect } from 'react-redux';
import { TakeQuiz } from '../../../../models/customTypes';
import { ExtractId } from '../../../../utils/ExtractId';
import QuizInfo from '../../Containers/QuizInfo/QuizInfo';
import SubmitDialog from '../../Containers/SubmitDialog/SubmitDialog';
import { useHistory } from 'react-router-dom';
import AppError from '../../SharedComponents/AppError/AppError';
import Loading from '../../SharedComponents/Loading/Loading';
import './SolveQuiz.css';
import * as actionTypes from '../../../../store/actionTypes';

const defaultProps: TakeQuiz = {
  id: 0,
  category: 0,
  userId: 0,
  title: '',
  questions: [],
  timelimit: 0,
};

type Props = {
  quiz: any;
  answers: number[];
  children?: React.ReactNode;
  resetState: Function;
};

const SolveQuiz: React.FC<Props> = (props): JSX.Element => {
  const quizId: string = ExtractId();
  const history = useHistory();
  const [open, setOpen] = React.useState(false);
  const [quiz, setQuiz]: [
    TakeQuiz,
    (categories: TakeQuiz) => void
  ] = React.useState(defaultProps);
  const [timer, setTimer] = React.useState(5);
  const [error, setError]: [string, (error: string) => void] = React.useState(
    ''
  );
  const [loading, setLoading]: [
    boolean,
    (loading: boolean) => void
  ] = React.useState<boolean>(true);

  useEffect(() => {
    const registerTake = httpProvider.get(`${BASE_URL}/takes/${quizId}`);
    const quizData = httpProvider.get(`${BASE_URL}/quiz/${quizId}`);

    Promise.all([registerTake, quizData]).then((resolved) => {
      const notValidTake = resolved[0].message;
      if (notValidTake) {
        setError(notValidTake);
      } else {
        const requestedQuiz = resolved[1];
        setQuiz({ ...requestedQuiz });
        // eslint-disable-next-line eqeqeq
        if (requestedQuiz.timelimit != 0) {
          setTimer(requestedQuiz.timelimit * 60);
        }
      }
      setLoading(false);
    });
  }, [quizId]);

  const id: any | null = React.useRef(null);
  const clear = () => {
    window.clearInterval(id.current);
  };

  React.useEffect(() => {
    id.current = window.setInterval(() => {
      setTimer((time) => time - 1);
    }, 1000);
    return () => clear();
  }, []);

  const forceSubmit = (data: number[]) => {
    httpProvider
      .put(`${BASE_URL}/takes/${quizId}`, { take_answers: data })
      .then((result) => {})
      .finally(() => {
        props.resetState();
        history.push('/dashboard');
      });
  };

  React.useEffect(() => {
    if (quiz.timelimit !== 0) {
      if (timer === -1) {
        clear();
        forceSubmit(props.answers);
      }
    }
  }, [timer]);

  window.addEventListener('beforeunload', (e) => {
    forceSubmit(props.answers);
  });

  useEffect(() => {
    return history.listen((location) => {
      forceSubmit(props.answers);
    });
  }, [history]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  if (loading) {
    return (
      <div>
        <Loading />
      </div>
    );
  }

  if (error) {
    return (
      <div>
        <AppError message={error} />
      </div>
    );
  }

  return (
    <div className='solveMain'>
      <SubmitDialog
        answers={props.answers}
        open={open}
        handleClose={handleClose}
        quizId={quizId}
        resetState={props.resetState}
      />
      <FormControl className='solveLeft'>
        {quiz.questions &&
          quiz.questions.map((question) => (
            <QuizQuestion key={question.id!} question={question} />
          ))}
        <ButtonGroup>
          <Button
            style={{ margin: '15px auto 15px auto' }}
            variant='contained'
            color='primary'
            size='large'
            onClick={() => handleClickOpen()}
          >
            Submit
          </Button>
        </ButtonGroup>
      </FormControl>
      <div className='solveRight'>
        <div className='solveRightT'>
          <QuizInfo
            title={quiz.title}
            questions={quiz.questions}
            id={quiz.id}
            userId={quiz.userId}
            category={quiz.category}
            timelimit={timer}
            totalTime={quiz.timelimit * 60}
            showtimer={quiz.timelimit === 0 ? false : true}
          />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state: any) => {
  return {
    answers: state.takeAnswers,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    resetState: () => dispatch({ type: actionTypes.RESET_STATE }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SolveQuiz);
