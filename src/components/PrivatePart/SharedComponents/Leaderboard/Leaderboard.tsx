import React, { useEffect } from 'react';
import httpProvider from '../../../../providers/http-provider';
import { BASE_URL } from '../../../../common/constants';
import { Leaderboard } from '../../../../models/customTypes';
import LeaderboardTable from '../../Containers/LeaderboardTable/LeaderboardTable';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { useCustomQueryParams } from '../../../../utils/custom-hooks/useCustomQueryParams';
import './Leaderboard.css';
import { Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import AppError from '../AppError/AppError';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: '80%',
        backgroundColor: 'transperant',
      },
    },
  })
);

const LeaderboardPage: React.FC = (): JSX.Element => {
  const classes = useStyles();
  const { page } = useCustomQueryParams();
  const defaultProps: Leaderboard = {
    result: [],
    count: 0,
    currentPage: 0,
    hasNext: false,
    hasPrevious: false,
  };

  const [results, setResults]: [
    Leaderboard,
    (students: Leaderboard) => void
  ] = React.useState(defaultProps);

  const [error, setError]: [string, (error: string) => void] = React.useState(
    ''
  );

  const [searchInput, setSearchInput]: [
    string,
    (value: string) => void
  ] = React.useState('');

  useEffect(() => {
    httpProvider
      .get(`${BASE_URL}/scores?page=${page}&search=${searchInput}`)
      .then((result: Leaderboard) => {
        setResults(result);
      })
      .catch((error) => setError(error.message))
      .finally();
  }, [searchInput, page]);

  if (error) {
    return (
      <div>
        <AppError message={error} />
      </div>
    );
  }

  return (
    <div className='leaderboardMain'>
      <div className='leaderboardLeft'>
        <LeaderboardTable props={results} />
        {results && results.hasPrevious ? (
          <Link
            to={`/leaderboard?page=${+page - 1}&search=${searchInput}`}
            style={{
              textDecoration: 'none',
              color: 'black',
            }}
          >
            <Button
              variant='contained'
              color='primary'
              style={{ margin: '15px' }}
            >
              Previous page
            </Button>
          </Link>
        ) : (
          <Button disabled style={{ margin: '15px' }}>
            Previous page
          </Button>
        )}

        {results && results.hasNext ? (
          <Link
            to={`/leaderboard?page=${+page + 1}&search=${searchInput}`}
            style={{
              textDecoration: 'none',
              color: 'black',
            }}
          >
            <Button
              variant='contained'
              color='primary'
              style={{ margin: '15px' }}
            >
              Next Page
            </Button>
          </Link>
        ) : (
          <Button disabled style={{ margin: '15px' }}>
            Next page
          </Button>
        )}
      </div>
      <div className='leaderboardRight'>
        <div className='leaderboardRightT'>
          <form className={classes.root} noValidate autoComplete='off'>
            <TextField
              onChange={(event: { target: { value: string } }) =>
                setSearchInput(event.target.value)
              }
              fullWidth
              id='outlined-search'
              label='Search'
              type='outlined'
              helperText='by username'
            />
          </form>
        </div>
      </div>
    </div>
  );
};

export default LeaderboardPage;
