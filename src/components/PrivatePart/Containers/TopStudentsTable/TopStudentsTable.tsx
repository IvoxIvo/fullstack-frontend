import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';
import { TopScores } from '../../../../models/customTypes';

type Props = {
  students: TopScores[];
};

const useStyles = makeStyles({
  table: {
    width: '100%',
  },
  header: {
    width: '100%',
    paddingTop: '15px',
  },
  break: {
    width: '40%',
  },
});

const TopStudentsTable: React.FC<Props> = ({ students }): JSX.Element => {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Typography
        className={classes.header}
        noWrap
        gutterBottom
        variant='h6'
        align='center'
      >
        Leaderboard
      </Typography>
      <hr className={classes.break} />
      <Table className={classes.table} size='small' aria-label='a dense table'>
        <TableHead>
          <TableRow>
            <TableCell align='left'>User name</TableCell>
            <TableCell align='left'>First name</TableCell>
            <TableCell align='left'>Last name</TableCell>
            <TableCell align='left'>Score</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {students.map((student) => (
            <TableRow key={student.username}>
              <TableCell component='th' scope='row'>
                {student.firstname}
              </TableCell>
              <TableCell align='left'>{student.firstname}</TableCell>
              <TableCell align='left'>{student.lastname}</TableCell>
              <TableCell align='left'>{student.result}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default TopStudentsTable;
