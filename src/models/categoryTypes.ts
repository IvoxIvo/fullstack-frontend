export type category = {
  description: string;
  id: number;
  images_id: number;
  name: string;
  path: string;
  users_id: number;
};

export type categoriesData = {
  category: category[];
  count: number;
  currentPage: number;
  hasNext: boolean;
  hasPrevious: boolean;
};

export type submitCategory = {
  name: string;
  description: string | undefined;
  images_id: string;
};
