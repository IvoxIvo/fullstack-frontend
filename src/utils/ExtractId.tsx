import { useLocation } from 'react-router-dom';

export const ExtractId = (): string => {
  const location: {
    pathname: string;
  } = useLocation();

  const urlAdress: string[] = location.pathname.split('/');
  const id: string = urlAdress[urlAdress.length - 1];

  return id;
};
