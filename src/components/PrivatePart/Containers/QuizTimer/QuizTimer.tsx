import React from 'react';
import CircularProgress, {
  CircularProgressProps,
} from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const CircularProgressWithLabel = (
  props: CircularProgressProps & { value: number; timelimit: any }
) => {
  return (
    <Box position='relative' display='inline-flex'>
      <CircularProgress
        variant='static'
        size='5rem'
        {...props}
        style={{ backgroundColor: 'white' }}
      />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position='absolute'
        display='flex'
        alignItems='center'
        justifyContent='center'
      >
        <Typography variant='body2' component='div'>
          {props.timelimit > 60
            ? `${Math.floor(props.timelimit / 60)}min
            ${props.timelimit % 60}sec`
            : `${props.timelimit % 60}sec`}
        </Typography>
      </Box>
    </Box>
  );
};

type Props = {
  timelimit: number;
  totalTime: number;
};

const QuizTimer: React.FC<Props> = ({ timelimit, totalTime }) => {
  const [progress, setProgress] = React.useState(
    +Math.floor((+timelimit / +totalTime) * 100)
  );
  const newValue = +Math.floor((+timelimit / +totalTime) * 100);
  React.useEffect(() => {
    const timer = setInterval(() => {
      return setProgress(newValue);
    }, 1000);
    return () => {
      clearInterval(timer);
    };
  }, [newValue]);

  return <CircularProgressWithLabel value={progress} timelimit={timelimit} />;
};

export default QuizTimer;
