import React, { useEffect, useState } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import httpProvider from '../../../../providers/http-provider';
import { BASE_URL } from '../../../../common/constants';
import { useAuth } from '../../../../auth/AuthContext';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles({
  table: {
    width: '100%',
  },
  break: {
    width: '80%',
  },
  header: {
    width: '100%',
    paddingTop: '15px',
  },
});

const LastQuizzes = () => {
  const classes = useStyles();
  const { user } = useAuth();
  const [quizzes, updateQuizzes] = useState<any[]>([]);

  useEffect(() => {
    httpProvider.get(`${BASE_URL}/quiz/user/${user?.sub}`).then((data) => {
      if (data.message) {
        console.log(data.message);
        return;
      }
      updateQuizzes(data);
    });
  }, []);

  return (
    <TableContainer component={Paper} className={classes.table}>
      <Typography className={classes.header} noWrap variant='h6' align='center'>
        The last 5 quizzes created by you
      </Typography>
      <hr className={classes.break} />
      <Table size='small' aria-label='a dense table'>
        <TableHead>
          <TableRow>
            <TableCell align='left'>#</TableCell>
            <TableCell align='left'>Quiz name</TableCell>
            <TableCell align='left'>Timelimit</TableCell>
            <TableCell align='left'>Category</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {quizzes &&
            quizzes.map((quiz, index) => (
              <TableRow key={quiz.id}>
                <TableCell align='left'>{index + 1}</TableCell>
                <TableCell align='left'>{quiz.title}</TableCell>
                <TableCell align='left'>{quiz.timelimit}</TableCell>
                <TableCell>{quiz.category}</TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default LastQuizzes;
