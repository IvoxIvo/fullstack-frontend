import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { CategoryInfoProps } from '../../../../models/customTypes';
import { BASE_URL } from '../../../../common/constants';

const useStyles = makeStyles({
  root: {
    margin: 2,
    flex: '1 0 31%',
  },
  media: {
    height: '220px',
    width: '220px',
    margin: '15px auto 0px auto',
    paddingTop: 'auto',
  },
});

const CategoryInfo: React.FC<CategoryInfoProps> = ({
  category,
}): JSX.Element => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={`${BASE_URL}/${category.path}`}
          title='Category Image'
        />
        <CardContent>
          <Typography gutterBottom variant='h5' component='h2'>
            {category.name}
          </Typography>
          {category.description ? (
            <Typography gutterBottom variant='subtitle1'>
              {category.description}
            </Typography>
          ) : null}
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default CategoryInfo;
