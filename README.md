# GotQuiz? Application


**GotQuiz?** is the final project for Telerik Academy - Alpha JavaScript Track (A24) - by Ivo Georgiev and Nikolai Mirchev.  
Project timeframe: 3 Nov 2020 - 3 Dec 2020

## Project Description
**GotQuiz?** is an application that can be used to create and solve quizzes online, both by students and teachers.

## Student Part
Students can register and take quizzes. Their scores are recorded and used to form a leaderboard. Each student can attempt each quiz only once, and check the history of their submissions. Some quizzes may need to be solved in a certain timeframe after their attempted.

## Teacher Part
Teachers are predefined users of the application who can create categories and quizzes.  
A category consists of the following:  
  - **name** - Required. Must be unique among categories.
  - **description** (optional) - A shot text about the category.
  - **cover image** - A limited selection of predefined images among which teachers can choose.  
  
A quiz consists of the following:  
  - **title** - The name of the quiz. Must be unique among all quizzes.
  - **timelimit** (optional) - A timeframe for solving the quiz. After the time is up the quiz is automatically submitted.
  - **questions** - At least two per quiz. Each question give between 1 and 6 points if answered correctly and must consists of at least two answers.
  - **answers** - Answers can be either multiple-choice or single-choice.

Teachers can also solve quizzes exactly like students do but their score is not recorded or put into the leaderboard.

## Technologies & Tools Used

### Front-end
  - React
  - Redux
  - Material UI
### Back-end
  - Node.js
  - Express.js
  - JWT
  - MariaDB

### Tools
  - TypeScript (Front-end & Back-end)
  - MySQL Workbench

## How to run the project
  - Database generation script - you can find it on the backend repository
  - Front-end - type ```npm start``` into the terminal to start the client
  - Back-end - type ```npm start``` into the terminal to start the server
