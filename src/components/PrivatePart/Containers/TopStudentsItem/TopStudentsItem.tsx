import React from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { TopStudentsItemProps } from '../../../../models/customTypes';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: '8px',
      width: 'auto',
      borderRadius: '8px',
      backgroundColor: theme.palette.background.paper,
    },
  })
);

const TopStudentsItem: React.FC<TopStudentsItemProps> = ({
  student,
}): JSX.Element => {
  const classes = useStyles();

  return (
    <List className={classes.root} aria-label='contacts'>
      <ListItem>
        <ListItemText primary={`User: ${student.username}`} />
        <ListItemText inset secondary={`Result:${student.result}`} />
      </ListItem>
    </List>
  );
};

export default TopStudentsItem;
