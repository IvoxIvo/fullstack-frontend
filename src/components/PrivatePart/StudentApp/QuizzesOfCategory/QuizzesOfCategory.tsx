import React, { useEffect } from 'react';
import httpProvider from '../../../../providers/http-provider';
import { BASE_URL } from '../../../../common/constants';
import MainItemContainer from '../../Containers/MainItemContainer/MainItemContainer';
import QuizCard from '../../Containers/QuizCard/QuizCard';
import AppError from '../../SharedComponents/AppError/AppError';
import Loading from '../../SharedComponents/Loading/Loading';

type Quizzes = {
  quiz_id: string;
  title: string;
  name: string;
  taken_quiz: number;
};

type Props = {
  key: string;
  id: string;
};

const QuizzesOfCategory: React.FC<Props> = ({ id }): JSX.Element => {
  const defaultProps: Quizzes[] = [];

  const [quizzes, setQuizzes]: [
    Quizzes[],
    (categories: Quizzes[]) => void
  ] = React.useState(defaultProps);

  const [loading, setLoading]: [
    boolean,
    (loading: boolean) => void
  ] = React.useState<boolean>(true);

  const [error, setError]: [string, (error: string) => void] = React.useState(
    ''
  );

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get(`${BASE_URL}/categories/quizzes/${id}`)
      .then((result) => {
        if (Array.isArray(result)) {
          setQuizzes([...result]);
        } else {
          throw new Error(result.message);
        }
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, [id]);

  if (error) {
    return (
      <div>
        <AppError message={error} />
      </div>
    );
  }

  if (loading) {
    return (
      <div>
        <Loading />
      </div>
    );
  }

  return (
    <MainItemContainer>
      {quizzes.length ? (
        quizzes.map((quiz) => <QuizCard key={quiz.quiz_id} quiz={quiz} />)
      ) : (
        <div>This category has no quizzes at the moment!</div>
      )}
    </MainItemContainer>
  );
};

export default QuizzesOfCategory;
