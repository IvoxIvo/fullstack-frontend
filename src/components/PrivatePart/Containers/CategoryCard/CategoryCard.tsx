import React from 'react';
import { Link } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { CategoryCardProps } from '../../../../models/customTypes';
import './CategoryCard.css';
import { BASE_URL } from '../../../../common/constants';

const CategoryCard: React.FC<CategoryCardProps> = ({
  category,
}): JSX.Element => {
  return (
    <Card className={'categoryCard'}>
      <Link
        to={`/category/${category.id}`}
        style={{ textDecoration: 'none', color: 'black' }}
      >
        <CardActionArea>
          <CardContent className={'categoryCardContent'}>
            <img
              className={'categoryCardMedia'}
              src={`${BASE_URL}/${category.path}`}
              alt='Logo'
            />
            <Typography
              variant='body1'
              style={{
                margin: 'auto',
                fontSize: '18px',
              }}
            >
              {category.name.toUpperCase()}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Link>
    </Card>
  );
};

export default CategoryCard;
