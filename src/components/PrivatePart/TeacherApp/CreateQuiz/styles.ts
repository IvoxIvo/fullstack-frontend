import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import { yellow } from '@material-ui/core/colors';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 480,
    },
    selectEmpty: {
      marginTop: theme.spacing(1),
    },
    root: {
      margin: theme.spacing(1),
      minWidth: 480,
    },
    timelimit: {
      margin: theme.spacing(1),
      maxWidth: 90,
    },
    category: {
      margin: theme.spacing(1),
      minWidth: 235,
    },
    points: {
      margin: theme.spacing(1),
      maxWidth: 60,
      marginLeft: '1.5rem',
      marginRight: '1.5rem',
    },
    question: {
      margin: theme.spacing(1),
      minWidth: 420,
    },
    answer: {
      margin: theme.spacing(1),
      minWidth: 400,
      marginRight: '2rem',
    },
    radioButton: {
      margin: theme.spacing(1),
      marginTop: '1.9rem',
    },
    iconDone: {
      color: green[500],
      paddingTop: '1.5rem',
      position: 'absolute',
    },
    iconInfo: {
      color: yellow['A700'],
      paddingTop: '1.5rem',
      position: 'absolute',
    },
    iconError: {
      color: 'red',
      paddingTop: '1.5rem',
      position: 'absolute',
    },
    hr: {
      maxWidth: 700,
    },
    p: {
      fontSize: '1.2rem',
    },
    errorMessage: {
      color: 'red',
      fontWeight: 'bold',
    },
    submitButtonStyle: {
      margin: 0,
      top: 'auto',
      right: 30,
      bottom: 85,
      left: 'auto',
      width: 130,
      position: 'fixed',
    },
    cancelButtonStyle: {
      margin: 0,
      top: 'auto',
      right: 30,
      bottom: 30,
      left: 'auto',
      width: 130,
      position: 'fixed',
    },
  })
);

export default useStyles;
