import React from 'react';
import Categories from '../../SharedComponents/Categories/Categories';
import LastTakes from '../LastTakes/LastTakes';
import TopStudents from '../TopStudents/TopStudents';
import './Dashboard.css';

const Dashboard: React.FC = (): JSX.Element => {
  return (
    <div className='dashMain'>
      <div className='dashLeft'>
        <Categories />
      </div>
      <div className='dashRight'>
        <div className='dashRightT'>
          <LastTakes />
        </div>
        <div className='dashRightT'>
          <TopStudents />
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
