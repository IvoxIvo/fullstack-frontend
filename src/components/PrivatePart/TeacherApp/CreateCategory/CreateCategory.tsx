import { Button, Grid, TextField } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { BASE_URL } from '../../../../common/constants';
import httpProvider from '../../../../providers/http-provider';
import useStyles from './styles';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';
import {
  category,
  categoriesData,
  submitCategory,
} from '../../../../models/categoryTypes';
import CategoryImages from '../CategoryImages/CategoryImages';
import { useHistory } from 'react-router-dom';
import CreateCategoryContainer from '../../Containers/CreateCategoryContainer/CreateCategoryContainer';

const CreateCategory = () => {
  const classes = useStyles();
  const [categoryName, setCategoryName] = useState<string>('');
  const [categoryNameError, setCategoryNameError] = useState<boolean>(true);
  const [categoryDescription, setCategoryDescription] = useState<string>('');
  const [categories, setCategories] = useState<string[]>([]);
  const [helperText, setHelperText] = useState<string | null>(null);
  const [imageId, setImageId] = useState<number>(1);
  const history = useHistory();

  useEffect(() => {
    httpProvider.get(`${BASE_URL}/categories`).then((data: categoriesData) => {
      const categoryArray = data.category.map((cat: category) =>
        cat.name.trim()
      );
      setCategories(categoryArray);
    });
  }, []);

  const handleCategoryName = (event: React.ChangeEvent<{ value: string }>) => {
    const name: string = event.target.value;
    const duplicate: boolean = categories.some(
      (category: string) => category === name
    );
    if (duplicate) {
      setCategoryNameError(true);
      setHelperText('A category with the same name already exists!');
    } else if (name.length > 2 && name.length < 45) {
      setCategoryNameError(false);
      setHelperText(null);
    } else {
      setCategoryNameError(true);
      setHelperText('Category name must be between 2 and 45 characters!');
    }
    setCategoryName(name);
  };

  const handleCategoryDescription = (
    event: React.ChangeEvent<{ value: string }>
  ) => {
    setCategoryDescription(event.target.value);
  };

  const onSubmit = () => {
    const sumbitData: submitCategory = {
      name: categoryName,
      description: categoryDescription,
      images_id: `${imageId}`,
    };

    httpProvider
      .put(`${BASE_URL}/categories`, sumbitData)
      .then((data) => console.log(data))
      .catch((error) => console.log(error))
      .finally(() => {
        history.push('/dashboard');
      });
  };

  const handleImageSelect = (id: number) => {
    setImageId(id);
  };

  return (
    <div className={classes.root}>
      <h3>Create Category</h3>
      <form>
        <Grid container spacing={1} direction='column' justify='center'>
          <Grid item>
            <TextField
              className={classes.root}
              label='Category name...'
              name='name'
              size='small'
              required
              value={categoryName}
              onChange={handleCategoryName}
              helperText={helperText}
            />
            {categoryNameError ? (
              <PriorityHighIcon className={classes.iconError} />
            ) : (
              <DoneOutlineIcon className={classes.iconDone} />
            )}
          </Grid>
          <Grid item>
            <TextField
              className={classes.root}
              label='Category description... (Optional)'
              name='description'
              size='small'
              multiline
              value={categoryDescription}
              onChange={handleCategoryDescription}
            />
          </Grid>
        </Grid>
        <Button
          variant='contained'
          color='primary'
          onClick={onSubmit}
          disabled={categoryNameError}
          style={{ marginTop: '25px' }}
        >
          Create Category
        </Button>
        <CreateCategoryContainer>
          <CategoryImages handleImageSelect={handleImageSelect} />
        </CreateCategoryContainer>
      </form>
    </div>
  );
};

export default CreateCategory;
