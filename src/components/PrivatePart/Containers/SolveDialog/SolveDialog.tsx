import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Typography from '@material-ui/core/Typography';
import { useTheme } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { SolveDialogProps } from '../../../../models/customTypes';

const SolveDialog: React.FC<SolveDialogProps> = (props): JSX.Element => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Dialog
      fullScreen={fullScreen}
      open={props.open}
      onClose={() => props.handleClose()}
      aria-labelledby='responsive-dialog-title'
      style={{ textAlign: 'center' }}
      disableBackdropClick
    >
      <DialogTitle id='responsive-dialog-title'>
        <Typography gutterBottom variant='h5' component='h2'>
          {'Before you take this quiz'}
        </Typography>
        <hr />
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          Please note that you can do this quiz just once! After submitting,
          your result will be displayed. Also have in mind that navigating to
          another page or refreshing the page of the quiz will end your take!
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <div
          style={{
            marginLeft: 'auto',
            marginRight: 'auto',
            marginBottom: '10px',
          }}
        >
          <Link
            to={`/quiz/${props.quizId}`}
            style={{
              textDecoration: 'none',
              color: 'black',
              marginRight: '12px',
            }}
          >
            <Button variant='contained' color='primary'>
              Take the quiz
            </Button>
          </Link>
          <Button
            style={{
              marginLeft: '12px',
            }}
            onClick={props.handleClose}
            color='primary'
            variant='outlined'
          >
            Cancel
          </Button>
        </div>
      </DialogActions>
    </Dialog>
  );
};
export default SolveDialog;
