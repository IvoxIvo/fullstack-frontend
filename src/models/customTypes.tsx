export type TakeQuiz = {
  id: number;
  category: number;
  userId: number;
  title: string;
  questions: TakeQuestion[];
  timelimit: number;
};

export type TakeQuestion = {
  id: number;
  name: string;
  points: number;
  answers: TakeAnswer[];
};

export type TakeAnswer = {
  id: number;
  text: string;
  is_correct: number;
};

export type Category = {
  id: string;
  users_id: string;
  name: string;
  description: string;
  images_id: string;
  path: string;
};

export type LastTake = {
  takes_id: string;
  quiz_id: string;
  users_id: string;
  started_at: string;
  is_submitted: string;
  result: string;
  username: string;
  firstname: string;
  lastname: string;
  title: string;
};

export type TopScores = {
  result: string;
  username: string;
  firstname: string;
  lastname: string;
};

export type Leaderboard = {
  result: LeaderboardList[];
  count: number;
  currentPage: number;
  hasNext: boolean;
  hasPrevious: boolean;
};

export type MyHistoryList = {
  takes_id: number;
  quiz_id: number;
  users_id: number;
  started_at: Date;
  is_submitted: number;
  result: number;
  username: string;
  firstname: string;
  lastname: string;
  title: string;
};

export type UserHistory = {
  result: MyHistoryList[];
  count: number;
  currentPage: number;
  hasNext: boolean;
  hasPrevious: boolean;
};

export type LeaderboardList = {
  firstname: string;
  lastname: string;
  username: string;
  result: number;
};
export type CategoryInfoProps = {
  key: number;
  category: {
    id: number;
    users_id: number;
    description: string;
    name: string;
    path: string;
  };
};

export type CategoriesData = {
  category: Category[];
  count: number;
  currentPage: number;
  hasNext: boolean;
  hasPrevious: boolean;
};

export type SubmitCategory = {
  name: string;
  description: string;
  images_id: string;
};

export type CategoryCardProps = {
  key: string;
  category: {
    id: string;
    users_id: string;
    name: string;
    description: string;
    images_id: string;
    path: string;
  };
};

export type QuizCardProps = {
  key: string;
  quiz: {
    quiz_id: string;
    title: string;
    name: string;
    taken_quiz: number;
  };
};

export type QuizInfoProps = {
  id: number;
  category: number;
  userId: number;
  title: string;
  questions: TakeQuestion[];
  timelimit: number;
  totalTime: number;
  showtimer: boolean;
};

export type SolveDialogProps = {
  handleClose(): void;
  open: boolean;
  quizId: string;
};

export type SubmitDialogProps = {
  resetState: Function;
  handleClose(): void;
  open: boolean;
  answers: number[];
  quizId: string;
};

export type Submit = {
  result: number | undefined;
  maxPoints: number | undefined;
};

export type TopStudentsItemProps = {
  key: number;
  student: {
    result: string;
    username: string;
    firstname: string;
    lastname: string;
  };
};
