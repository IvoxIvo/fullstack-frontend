import React from 'react';
import { connect } from 'react-redux';
import Checkbox from '@material-ui/core/Checkbox';
import { Typography, FormControlLabel } from '@material-ui/core';
import { TakeAnswer } from '../../../../models/customTypes';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import * as actionTypes from '../../../../store/actionTypes';

type Props = {
  updateTakeAnswers(id: number): void;
  key: number;
  answer: TakeAnswer;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    answerText: {
      margin: theme.spacing(1),
      width: '60%',
      backgroundColor: '#fff',
    },
  })
);

const QuizAnswer: React.FC<Props> = ({
  answer,
  updateTakeAnswers,
}): JSX.Element => {
  const classes = useStyles();

  return (
    <Typography noWrap>
      <FormControlLabel
        className={classes.answerText}
        control={
          <Checkbox
            onClick={() => updateTakeAnswers(answer.id)}
            name='checked'
            color='primary'
          />
        }
        label={answer.text}
      />
    </Typography>
  );
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    updateTakeAnswers: (answerId: number) =>
      dispatch({ type: actionTypes.UPDATE_TAKE_ANSWERS, value: answerId }),
  };
};

export default connect(null, mapDispatchToProps)(QuizAnswer);
