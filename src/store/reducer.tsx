import _ from 'lodash';
import * as actionTypes from './actionTypes';

type valid = {
  text?: boolean;
  title?: boolean;
  unique?: boolean;
  name?: boolean;
  points?: boolean;
  questions?: boolean;
  correctAnswers?: boolean;
  timelimit?: boolean;
  category?: boolean;
};

type answer = {
  valid: valid;
  answerId: number;
  questionId: number;
  text: string;
  is_correct: number;
};
type question = {
  valid: valid;
  markedAnswers: number;
  questionId: number;
  name: string;
  points: number;
  answers: answer[];
};
type quiz = {
  valid: valid;
  title: string;
  category: number | boolean;
  userId: number;
  timelimit: number | boolean;
  questions: question[];
};

type state = {
  quiz: quiz;
  takeAnswers: number[];
};

const initialState: state = {
  quiz: {
    valid: {
      title: false,
      timelimit: true,
      category: false,
      questions: false,
      unique: true,
    },
    title: '',
    category: false,
    userId: 0,
    timelimit: 0,
    questions: [],
  },
  takeAnswers: [],
};

const reducer = (
  state = initialState,
  action: { type: string; value: any }
) => {
  const copiedState = _.cloneDeep(state);
  let questionIndex: number;
  let answerIndex: number;

  switch (action.type) {
    case actionTypes.QUIZ_UPDATE_CATEGORY_ID:
      copiedState.quiz.category = action.value;
      copiedState.quiz.valid.category = true;
      return copiedState;

    case actionTypes.QUIZ_UPDATE_TITLE:
      copiedState.quiz.title = action.value.title;
      copiedState.quiz.valid.title = action.value.valid;
      return copiedState;

    case actionTypes.QUIZ_UPDATE_TIMELIMIT:
      copiedState.quiz.timelimit = action.value.timelimit;
      copiedState.quiz.valid.timelimit = action.value.valid;
      return copiedState;

    case actionTypes.QUIZ_UPDATE_USERID:
      copiedState.quiz.userId = action.value;
      return copiedState;

    case actionTypes.QUIZ_ADD_QUESTION:
      const newQuestion: question = {
        valid: { name: false, points: true, correctAnswers: false },
        markedAnswers: 0,
        questionId: action.value.questionId,
        name: '',
        points: 1,
        answers: [],
      };
      copiedState.quiz.questions.push(newQuestion);
      if (copiedState.quiz.questions.length >= 2) {
        copiedState.quiz.valid.questions = true;
      } else {
        copiedState.quiz.valid.questions = false;
      }
      return copiedState;

    case actionTypes.QUIZ_REMOVE_LAST_QUESTION:
      copiedState.quiz.questions.pop();
      if (copiedState.quiz.questions.length >= 2) {
        copiedState.quiz.valid.questions = true;
      } else {
        copiedState.quiz.valid.questions = false;
      }
      return copiedState;

    case actionTypes.QUIZ_DUPLICATE_NAME:
      copiedState.quiz.valid.unique = action.value.valid;
      return copiedState;

    case actionTypes.QUESTION_ADD_ANSWER:
      questionIndex = action.value.questionId - 1;
      const newAnswer: answer = {
        valid: { text: false },
        questionId: action.value.questionId,
        answerId: action.value.answerId,
        text: '',
        is_correct: 0,
      };
      copiedState.quiz.questions[questionIndex].answers.push(newAnswer);
      return copiedState;

    case actionTypes.QUESTION_ADD_POINTS:
      questionIndex = action.value.questionId - 1;
      copiedState.quiz.questions[questionIndex].points = action.value.points;
      copiedState.quiz.questions[questionIndex].valid.points =
        action.value.valid;
      return copiedState;

    case actionTypes.QUESTION_ADD_TITLE:
      questionIndex = action.value.questionId - 1;
      copiedState.quiz.questions[questionIndex].name = action.value.name;
      copiedState.quiz.questions[questionIndex].valid.name = action.value.valid;
      return copiedState;

    case actionTypes.QUESTION_REMOVE_LAST_ANSWER:
      questionIndex = action.value - 1;
      const lastIndex =
        copiedState.quiz.questions[questionIndex].answers.length - 1;
      const lastAnswer =
        copiedState.quiz.questions[questionIndex].answers[lastIndex];
      const markedAnswers =
        copiedState.quiz.questions[questionIndex].markedAnswers;
      if (lastAnswer.is_correct && markedAnswers) {
        copiedState.quiz.questions[questionIndex].markedAnswers -= 1;
      }
      copiedState.quiz.questions[questionIndex].answers.pop();
      return copiedState;

    case actionTypes.ANSWER_ADD_TEXT:
      questionIndex = action.value.questionId - 1;
      answerIndex = action.value.answerId - 1;
      copiedState.quiz.questions[questionIndex].answers[answerIndex].text =
        action.value.text;
      copiedState.quiz.questions[questionIndex].answers[
        answerIndex
      ].valid.text = action.value.valid;
      return copiedState;

    case actionTypes.ANSWER_SET_SINGLE_CORRECT:
      questionIndex = action.value.questionId - 1;
      answerIndex = action.value.answerId - 1;
      copiedState.quiz.questions[questionIndex].markedAnswers = 1;
      copiedState.quiz.questions[questionIndex].answers.forEach((answer) => {
        if (action.value.answerId === answer.answerId) {
          answer.is_correct = 1;
        } else {
          answer.is_correct = 0;
        }
      });
      return copiedState;
    case actionTypes.ANSWER_SET_MULTI_CORRECT:
      questionIndex = action.value.questionId - 1;
      answerIndex = action.value.answerId - 1;
      if (action.value.is_correct) {
        copiedState.quiz.questions[questionIndex].markedAnswers += 1;
      } else {
        copiedState.quiz.questions[questionIndex].markedAnswers -= 1;
      }
      copiedState.quiz.questions[questionIndex].answers[
        answerIndex
      ].is_correct = action.value.is_correct;
      return copiedState;

    case actionTypes.UPDATE_TAKE_ANSWERS:
      if (copiedState.takeAnswers.includes(action.value)) {
        return {
          ...copiedState,
          takeAnswers: [
            ...copiedState.takeAnswers.filter(
              (e: number) => e !== action.value
            ),
          ],
        };
      } else {
        return {
          ...copiedState,
          takeAnswers: [...copiedState.takeAnswers, action.value],
        };
      }
    case actionTypes.VALIDATE_CORRECT_ANSWERS:
      questionIndex = action.value.questionId - 1;
      const questionType = action.value.questionType;
      const correctAnswers = copiedState.quiz.questions[
        questionIndex
      ].answers.reduce((correct: number, value: answer) => {
        if (value.is_correct) {
          correct++;
        }
        return correct;
      }, 0);

      const questionHasValidAnswers =
        copiedState.quiz.questions[questionIndex].valid;
      if (questionType === 'single' && correctAnswers < 1) {
        questionHasValidAnswers.correctAnswers = false;
      } else if (questionType === 'single' && correctAnswers > 0) {
        questionHasValidAnswers.correctAnswers = true;
      } else if (questionType === 'multi' && correctAnswers < 2) {
        questionHasValidAnswers.correctAnswers = false;
      } else if (questionType === 'multi' && correctAnswers > 1) {
        questionHasValidAnswers.correctAnswers = true;
      }
      return copiedState;
    case actionTypes.RESET_STATE:
      return initialState;
  }
  return state;
};

export default reducer;
