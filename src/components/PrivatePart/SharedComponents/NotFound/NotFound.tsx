import React from 'react';

const NotFound: React.FC = (): JSX.Element => {
  return <>404 Not Found...</>;
};

export default NotFound;
