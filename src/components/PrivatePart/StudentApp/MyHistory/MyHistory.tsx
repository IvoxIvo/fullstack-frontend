import React, { useEffect } from 'react';
import httpProvider from '../../../../providers/http-provider';
import { BASE_URL } from '../../../../common/constants';
import { UserHistory } from '../../../../models/customTypes';
import MyHistoryTable from '../../Containers/MyHistoryTable/MyHistoryTable';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { useCustomQueryParams } from '../../../../utils/custom-hooks/useCustomQueryParams';
import './MyHistory.css';
import { Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import AppError from '../../SharedComponents/AppError/AppError';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: '80%',
        backgroundColor: 'transperant',
      },
    },
  })
);

const MyHistory: React.FC = (): JSX.Element => {
  const classes = useStyles();
  const { page } = useCustomQueryParams();
  const defaultProps: UserHistory = {
    result: [],
    count: 0,
    currentPage: 0,
    hasNext: false,
    hasPrevious: false,
  };

  const [results, setResults]: [
    UserHistory,
    (data: UserHistory) => void
  ] = React.useState(defaultProps);

  const [error, setError]: [string, (error: string) => void] = React.useState(
    ''
  );

  const [searchInput, setSearchInput]: [
    string,
    (value: string) => void
  ] = React.useState('');

  useEffect(() => {
    httpProvider
      .get(`${BASE_URL}/takes/student/all?page=${page}&search=${searchInput}`)
      .then((result: UserHistory) => {
        setResults(result);
      })
      .catch((error) => setError(error.message))
      .finally();
  }, [searchInput, page]);

  if (error) {
    return (
      <div>
        <AppError message={error} />
      </div>
    );
  }

  return (
    <div className='myhistoryMain'>
      <div className='myhistoryLeft'>
        <MyHistoryTable props={results} />
        {results && results.hasPrevious ? (
          <Link
            to={`/myhistory?page=${+page - 1}&search=${searchInput}`}
            style={{
              textDecoration: 'none',
              color: 'black',
              margin: '15px',
            }}
          >
            <Button
              variant='contained'
              color='primary'
              style={{ margin: '15px' }}
            >
              Previous page
            </Button>
          </Link>
        ) : (
          <Button disabled>Previous page</Button>
        )}

        {results && results.hasNext ? (
          <Link
            to={`/myhistory?page=${+page + 1}&search=${searchInput}`}
            style={{
              textDecoration: 'none',
              color: 'black',
            }}
          >
            <Button
              variant='contained'
              color='primary'
              style={{ margin: '15px' }}
            >
              Next Page
            </Button>
          </Link>
        ) : (
          <Button disabled style={{ margin: '15px' }}>
            Next page
          </Button>
        )}
      </div>
      <div className='myhistoryRight'>
        <div className='myhistoryRightT'>
          <form className={classes.root} noValidate autoComplete='off'>
            <TextField
              onChange={(event: { target: { value: string } }) =>
                setSearchInput(event.target.value)
              }
              fullWidth
              id='outlined-search'
              label='Search'
              type='outlined'
              helperText='by quiz title'
            />
          </form>
        </div>
      </div>
    </div>
  );
};

export default MyHistory;
