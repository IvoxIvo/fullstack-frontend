import React from 'react';
import { LastTake } from '../../../../models/customTypes';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';

type Props = {
  takes: LastTake[];
};

const useStyles = makeStyles({
  table: {
    width: '100%',
  },
  header: {
    width: '100%',
    paddingTop: '15px',
  },
  break: {
    width: '45%',
  },
});

const LastTakeTable: React.FC<Props> = ({ takes }): JSX.Element => {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Typography
        className={classes.header}
        noWrap
        gutterBottom
        variant='h6'
        align='center'
      >
        Your last takes
      </Typography>
      <hr className={classes.break} />
      <Table className={classes.table} size='small' aria-label='a dense table'>
        <TableHead>
          <TableRow>
            <TableCell>Quiz Title</TableCell>
            <TableCell align='right'>Score</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {takes.map((take) => (
            <TableRow key={take.title}>
              <TableCell component='th' scope='row'>
                {take.title}
              </TableCell>
              <TableCell align='right'>{take.result}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default LastTakeTable;
