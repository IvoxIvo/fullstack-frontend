export type valid = {
  text?: boolean;
  title?: boolean;
  unique?: boolean;
  name?: boolean;
  points?: boolean;
  questions?: boolean;
  correctAnswers?: boolean;
  timelimit?: boolean;
  category?: boolean;
};

export type answer = {
  valid: valid;
  answerId: number;
  questionId: number;
  answerType: string;
  text: string;
  is_correct: number;
};
export type question = {
  valid: valid;
  markedAnswers: number;
  questionId: number;
  questionType: string;
  name: string;
  points: number;
  answers: answer[];
};
export type quiz = {
  valid: valid;
  title: string;
  category: number;
  userId: number;
  timelimit: number;
  questions: question[];
};

export type quizFormProps = {
  updateQuizCategoryId: Function;
  updateQuizTitle: Function;
  updateQuizTimelimit: Function;
  updateUserId: Function;
  addQuizQuestion: Function;
  removeQuizLastQuestion: Function;
  setDuplicateQuizName: Function;
  resetState: Function;
  quiz: quiz;
};

export type questionProps = {
  questionId: number;
  questionType: string;
  addQuestionAnswer: Function;
  addQuestionPoints: Function;
  addQuestionTitle: Function;
  removeQuestionLastAnswer: Function;
  validateCorrectAnswers: Function;
};

export type answerProps = {
  answerId: number;
  questionId: number;
  answerType: string;
  addAnswerText: Function;
  setMultiAnswerCorrect: Function;
  setSingleAnswerCorrect: Function;
  validateCorrectAnswers: Function;
};
