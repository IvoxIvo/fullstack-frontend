import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Typography from '@material-ui/core/Typography';
import httpProvider from '../../../../providers/http-provider';
import { useTheme } from '@material-ui/core/styles';
import { BASE_URL } from '../../../../common/constants';
import { Link } from 'react-router-dom';
import CircularProgress, {
  CircularProgressProps,
} from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import { SubmitDialogProps, Submit } from '../../../../models/customTypes';

const defaultSubmit: Submit = {
  result: 0,
  maxPoints: 0,
};

const SubmitDialog: React.FC<SubmitDialogProps> = (props): JSX.Element => {
  const [submit, setSubmit]: [Submit, (data: Submit) => void] = React.useState(
    defaultSubmit
  );

  const CircularProgressWithLabel = (
    props: CircularProgressProps & { value: number }
  ) => {
    return (
      <div>
        <Box position='relative' display='inline-flex'>
          <CircularProgress variant='static' size='6rem' {...props} />
          <Box
            top={0}
            left={0}
            bottom={0}
            right={0}
            position='absolute'
            display='flex'
            alignItems='center'
            justifyContent='center'
          >
            <Typography
              variant='h5'
              component='div'
              color='textSecondary'
            >{`${Math.round(props.value)}%`}</Typography>
          </Box>
        </Box>
      </div>
    );
  };

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const handleSubmit = (data: number[]) => {
    httpProvider
      .put(`${BASE_URL}/takes/${props.quizId}`, { take_answers: data })
      .then((result) => {
        setSubmit({ ...result });
      })
      .finally();

    props.resetState();
  };

  return (
    <Dialog
      fullScreen={fullScreen}
      open={props.open}
      onClose={() => props.handleClose()}
      aria-labelledby='responsive-dialog-title'
      style={{ textAlign: 'center' }}
      disableBackdropClick
    >
      {submit.maxPoints === 0 ? (
        <>
          <DialogTitle id='responsive-dialog-title'>
            <Typography gutterBottom variant='h5' component='h2'>
              {'Are you sure that you want to submit?'}
            </Typography>
            <hr />
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              Please note that you can do this quiz just once. The answers you
              chose can't be changed.Unanswered questions will be marked as
              wrong answers
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <div
              style={{
                marginLeft: 'auto',
                marginRight: 'auto',
                marginBottom: '10px',
              }}
            >
              <Button
                onClick={() => handleSubmit(props.answers)}
                color='primary'
                variant='contained'
                style={{
                  marginRight: '12px',
                }}
              >
                Submit
              </Button>
              <Button
                onClick={props.handleClose}
                color='primary'
                variant='outlined'
                style={{
                  marginLeft: '12px',
                }}
              >
                Cancel
              </Button>
            </div>
          </DialogActions>
        </>
      ) : (
        <>
          <DialogTitle id='responsive-dialog-title'>
            <Typography gutterBottom variant='h5' component='h2'>
              {'Your quiz is submitted'}
            </Typography>
            <hr />
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <CircularProgressWithLabel
                value={(+submit.result! / +submit.maxPoints!) * 100}
              />
              <Typography
                gutterBottom
                variant='h5'
                component='h2'
                style={{ marginTop: '15px' }}
              >
                {`Your score:${submit.result}`}
              </Typography>
              <Typography gutterBottom variant='h5' component='h2'>
                {`Maximum points:${submit.maxPoints}`}
              </Typography>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Link
              to={`/dashboard`}
              style={{
                textDecoration: 'none',
                color: 'black',
                margin: '10px auto 15px auto',
              }}
            >
              <Button color='primary' variant='contained'>
                Dashboard
              </Button>
            </Link>
          </DialogActions>
        </>
      )}
    </Dialog>
  );
};
export default SubmitDialog;
