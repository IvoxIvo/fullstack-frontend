import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Login from './Login/Login';
import Register from './Register/Register';
import './PublicApp.css';

const PublicApp: React.FC = () => {
  return (
    <div className='main-content-public'>
      <BrowserRouter>
        <Switch>
          <Redirect path='/' exact to='/login' />
          <Route path='/login' component={Login} />
          <Route path='/register' component={Register} />
          <Route path='*' component={Login} />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default PublicApp;
