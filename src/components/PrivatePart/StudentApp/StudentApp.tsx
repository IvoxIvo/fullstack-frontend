import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import SingleCategory from './SingleCategory/SingleCategory';
import Dashboard from './Dashboard/Dashboard';
import Leaderboard from '../SharedComponents/Leaderboard/Leaderboard';
import MyHistory from './MyHistory/MyHistory';
import SolveQuiz from './SolveQuiz/SolveQuiz';

import NavBar from '../SharedComponents/NavBar/NavBar';

const StudentApp: React.FC = (): JSX.Element => {
  return (
    <BrowserRouter>
      <NavBar />
      <div className='main-content'>
        <Switch>
          <Redirect path='/' exact to='/dashboard' />
          <Redirect path='/login' exact to='/dashboard' />
          <Route path='/dashboard' component={Dashboard} />
          <Route path='/category/:id' component={SingleCategory} />
          <Route path='/quiz/:id' component={SolveQuiz} />
          <Route path='/leaderboard' component={Leaderboard} />
          <Route path='/myhistory' component={MyHistory} />
          <Route path='*' component={Dashboard} />
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export default StudentApp;
