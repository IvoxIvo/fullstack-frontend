import React, { useEffect, useState } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import httpProvider from '../../../../providers/http-provider';
import { BASE_URL } from '../../../../common/constants';
import { useParams } from 'react-router-dom';
import { Button } from '@material-ui/core';
import { useHistory } from 'react-router';
import useStyles from './styles';

const ViewQuizInfo = () => {
  const classes = useStyles();
  const [submissions, setSubmissions] = useState<any[]>([]);
  const { id }: { id: string | undefined } = useParams();
  const history = useHistory();

  useEffect(() => {
    httpProvider.get(`${BASE_URL}/quiz/submissions/${id}`).then((data) => {
      setSubmissions(data);
    });
  }, []);
  return (
    <>
      {submissions.length ? (
        <TableContainer component={Paper} className={classes.tableContainer}>
          <Table
            className={classes.table}
            size='small'
            aria-label='a dense table'
          >
            <TableHead>
              <TableRow>
                <TableCell align='left'>#</TableCell>
                <TableCell align='left'>Username</TableCell>
                <TableCell align='left'>Name</TableCell>
                <TableCell align='left'>Date</TableCell>
                <TableCell align='left'>Score</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {submissions.map((take, index) => (
                <TableRow key={take.username}>
                  <TableCell align='left'>{index + 1}</TableCell>
                  <TableCell align='left'>{take.username}</TableCell>
                  <TableCell align='left'>{`${take.firstname} ${take.lastname}`}</TableCell>
                  <TableCell align='left'>
                    {new Date(take.started_at).toDateString()}
                  </TableCell>
                  <TableCell align='left'>{take.score}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      ) : (
        <div>This quiz has no submission so far...</div>
      )}
      <br />
      <Button
        variant='contained'
        size='large'
        color='primary'
        onClick={() => history.goBack()}
      >
        Back
      </Button>
    </>
  );
};

export default ViewQuizInfo;
